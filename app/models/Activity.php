<?php

class Activity
{
    protected $url = 'http://www.boredapi.com/api/activity';

    public $params = [
        'key'               => '',
        'type'              => '',
        'participants'      => '',
        'price'             => '',
        'minprice'          => '',
        'maxprice'          => '',
        'accessibility'     => '',
        'minaccessibility'  => '',
        'maxaccessibility'  => ''
    ];

    public function get()
    {
        $json = file_get_contents($this->get_query());

        $data = json_decode($json, true);

        if (!isset($data['error'])) return $data;
    }

    public function get_query()
    {    
        $params = $this->params;

        foreach ($params as &$param) unset($param);

        return $this->url.'?'.http_build_query($params);
    }

    public function set_query($ins = [])
    {
        foreach ($ins as $key => $value) {
            if (isset($this->params[$key])) {
                $this->params[$key] = $value;
            }
        }

        return $this;
    }
}
