<?php

class Home extends Controller
{
    public function index()
    {
        $title = 'What to do?!';
        
        $activity = (new Activity)->set_query($_REQUEST)->get();

        $this->view('home/index' , compact('title', 'activity'));
    }
}