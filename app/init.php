<?php

spl_autoload_register(function ($class) {
    $dirs = [
        'controllers/',
        'core/',
        'models/'
    ];
    foreach ($dirs as $dir) {
        if (file_exists('app/' . $dir . $class . '.php')) {
            require_once $dir . $class . '.php';
        }
    }
});