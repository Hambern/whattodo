<?php include('partials/top.php') ?>

<div class="container text-center px-3 py-5">
    <?php if ($activity) : ?>
        <section class="jello-vertical transparent-bg rounded p-3 my-3">
            <h1 class="display-5">
                <?= $activity['activity'] ?>
            </h1>
            <?php if (!empty($activity['link'])) : ?>
                <a class="mt-4" href="<?= $activity['link'] ?>" target="_blank">
                    <?= $activity['link'] ?>
                </a>
            <?php endif ?>
        </section>
    <?php endif ?>
    <form method="get">
        <button class="btn btn-dark btn-lg">
            <i class="bi bi-arrow-clockwise"></i> Another one!
        </button>
    </form>
</div>

<!--
<code>
    <pre>
        <?= print_r($activity, true) ?>
    </pre>
</code>
-->

<?php include('partials/bottom.php') ?>