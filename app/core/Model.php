<?php

class Model
{
    protected $db;

    protected $table;

    public function __construct()
    {
        $servername = 'localhost';
        $username   = 'student';
        $password   = 'student';
        $database   = 'student_select';
        $port       = 3690;

        $this->db = new mysqli($servername, $username, $password, $database, $port);

        if ($error = $this->db->connect_error)
            die("Connection failed: " . $error);
    }


    public function execute(string $sql, array $data = [])
    {
        $stmt = $this->db->prepare($sql);

        if (!empty($data)) {
            $stmt->bind_param(str_repeat('s', count($data)), ...$data);
        }
        
        $stmt->execute();
        
        return $stmt;
    }

    public function find(int $id)
    {
        $data = $this->execute("SELECT * FROM $this->table WHERE id = ?", [$id])->get_result()->fetch_all(MYSQLI_ASSOC);

        return $data ? $data[0] : false;
    }

    public function all()
    {
        return $this->execute("SELECT * FROM $this->table")->get_result()->fetch_all(MYSQLI_ASSOC);
    }

 }