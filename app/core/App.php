<?php

class App
{
    protected $controller = 'home';

    protected $method = 'index';

    protected $params = [];

    public function __construct()
    {
        $url = isset($_GET['url']) ? explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL)) : [];

        if (isset($url[0]) && class_exists($url[0])) {
            $this->controller = array_shift($url);
        }

        $this->controller = new $this->controller;

        if (isset($url[0]) && method_exists($this->controller, $url[0])) {
            $this->method = array_shift($url);
        }
        
        $this->params = $url;

        call_user_func_array([$this->controller, $this->method], $this->params);
    }
}