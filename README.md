# Whattodo

This simple web application simply gives you advice on what to do. It uses [The Bored API](https://www.boredapi.com/). See it in action here: <https://student.oedu.se/~mh6802/whattodo/>
